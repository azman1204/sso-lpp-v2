<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IdentityApp extends Model {
    protected $table = 'identity_app';

    function myapp() {
        return $this->belongsTo(\App\Models\App::class, 'app_id', 'id');
       }
}