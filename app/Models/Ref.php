<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ref extends Model {
    //return array
    public static function getRef($cat) {
        return self::where('cat', $cat)->pluck('name', 'code');
    }
}