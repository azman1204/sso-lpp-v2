<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Audit;

class Pakaudit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $audit          = new Audit();
        $audit->ip      = $request->ip();
        $path           = $request->path();
        $data           = $request->all();

        if ($path == 'auth' || $path == 'identity/save') {
            unset($data['password']);
            unset($data['password_confirmation']);
        }

        $audit->uri     = $path;
        $audit->method  = $request->isMethod('post') ? 'POST' : 'GET';
        $audit->data    = json_encode($data);
        $audit->save();
        return $next($request);
    }
}
