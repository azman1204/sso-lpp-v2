<?php
namespace App\Http\Controllers;
use App\Models\IdentityApp;

class DashboardController extends Controller {
    function index(){
        $apps = IdentityApp::where('identity_id', \Auth::user()->id)->get();
        //dd($apps);
        $user_id = urlencode($this->hashString(\Auth::user()->user_id));
        return view('dashboard.index', compact('apps', 'user_id'));
    }

    function hashString($data, $method='encrypt') {
        $secret = '123456';
        $iv = substr(hash('sha256', $secret), 0, 16);

        if ($method == 'encrypt') {
            $str = openssl_encrypt($data, "AES-256-CBC", "$secret", 0, $iv);
        } else if ($method == 'decrypt'){
            $str = openssl_decrypt($data, "AES-256-CBC", "123456",0, $iv);
        }
        return $str;
    }
}