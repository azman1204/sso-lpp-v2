<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Identity;
use Illuminate\Support\Facades\Hash;
use App\Models\App;
use App\Models\IdentityApp;

class IdentityController extends Controller
{
    function __construct() {
        $this->middleware('pakadmin');
    }

    //Function papar senarai pengguna


    //list all adentities
    function list(Request $req) {
        if ($req->has('name')) {
            $query = Identity::whereNotNull('id');
            if (! empty($req->name)) {
                $nama = $req->name;
                $query = $query->where('name','like', "%$nama%");
            }

            if (! empty($req->user_id)) {
                $user_id = $req->user_id;
                $query = $query->where('user_id','like', "%$user_id%");
            }
            $indentities = $query->paginate(20);
        } else {
            $indentities = Identity::paginate(20);
        }

        return view('identity.list', compact('indentities'));
    }

    // show create form
    function create() {
        $identity = new Identity();
        $apps = App::all();
        $iapp = []; // app yg user ini ada access
        return view('identity.form', compact('identity', 'apps', 'iapp'));
    }

    // edit
    function edit($id) {
        $identity = Identity::find($id);
        $apps = App::all();
        // senarai app id yg boleh dicapai oleh seseorang pengguna
        $iapp = IdentityApp::where('identity_id', $id)->pluck('app_id')->toArray();
        //dd($iapp);
        return view('identity.form', compact('identity', 'apps', 'iapp'));
    }

    // save / update
    function save(Request $request) {
        $id = $request->id;
        $rules = [
            'name'      =>'required|min:6|max:50',
            'email'     =>'required|email',
            'password'  =>'required|min:6|max:20|confirmed', //check pass
        ];

        //dd($request->all());
        if (empty($id)) {
            //insert
            $identity = new Identity();
            $rules['user_id'] = 'required|unique:identity,user_id';
            $identity->password = Hash::make($request->password);
            $identity->created_by = auth()->user()->id;

        }else{
            //update
            $rules['user_id'] = 'required';
            $identity = Identity::find($id);
            if ($request->password !== '********') {
                //user update password
                 $identity->password = Hash::make($request->password);
            }
            $identity->updated_by = auth()->user()->id;

        }

        $identity->user_id      = $request->user_id;
        $identity->name         = $request->name;
        $identity->email        = $request->email;
       // $identity->password     = Hash::make($request->password); //$request->password;
        $identity->created_by   = \Auth::user()->id;
        $identity->role         = $request->role;
        //validation, kalu x lepas validation akan auto redirect ke form sebelumnay
        $request->validate($rules);
        $identity->save();

        //clearkan capaian app user
        IdentityApp::where('identity_id', $identity->id)->delete();

        //set semula capaian
        $apps = $request->has('app') ? $request->app:[];
        foreach ($apps as $id) {
            $iapp = new IdentityApp();
            $iapp->identity_id = $identity->id;
            $iapp->app_id = $id;
            $iapp->save();

        }
        return redirect('identity/list');

    }

    // delete
    function delete($id) {
        Identity::find($id)->delete();
        return redirect('identity/list');
    }
}
