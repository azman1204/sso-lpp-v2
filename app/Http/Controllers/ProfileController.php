<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Models\Identity;

class ProfileController extends Controller {

    function forgot() {
        return view('profile.forgot');
    }

    function reset(Request $request) {
        $user_id = $request->user_id;
        $identity = \App\Models\Identity::where('user_id', $user_id)->first();
        if (!$identity) {
            return redirect('/forgot-password')->with('msg', 'User does not Exist');
        }else{
            // Send email Here & Generate Toke
            $token = base64_encode($user_id . date('Y-m-d H:i:s') . 'secret-key');
            $identity->token = $token;
            $identity->save();

           $data = ['identity' => $identity, 'token' => $token];
           Mail::send('profile.email', $data, function ($m) use ($identity) {
               $m->from("admin-sso@lpp.gov.my");
               $m->to($identity->email)->subject('Sila Tukar Katalaluan Anda');
           });

           return redirect('login')->with('msg', 'Sila Semak Email Anda... ');

        }
    }

    function change($token) {
        $identity = Identity::where('token', $token)->first();
        if (! $identity) {
            echo 'TIADA KEBENARAN ...';
            exit;
        }

        return view('profile.change', compact('identity', 'token'));
    }

    function update(Request $request) {
        $rules = [
            'password' => 'required|min:6|max:20|confirmed',
        ];
        $request->validate($rules);
        $identity = Identity::where('token', $request->token)->first();
        $identity->password = Hash::make($request->password);
        $identity->token = null;
        $identity->save();
        return redirect('/login')->with('msg', 'Katalaluan telah berjaya dikemaskini');

    }

    function resetPassword() {
        return view('profile.reset');
    }

    function resetHandler(Request $request) {
        $rules = [
            'password' => 'required|min:6|max:20|confirmed',
        ];
        $request->validate($rules);
        $identity = \Auth::user();
        $identity->password = Hash::make($request->password);
        $identity->save();
        return redirect('/login')->with('msg', 'Katalaluan telah berjaya dikemaskini');

    }
}

