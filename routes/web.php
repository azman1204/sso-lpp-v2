<?php
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\IdentityController;
use App\Http\Controllers\PasswordController;
use App\Http\Controllers\DashboardController;

Route::middleware(['pakaudit', 'pakguard'])->group(function () {
    Route::any('/identity/list', [IdentityController::class, 'list']);
    Route::get('/identity/create', [IdentityController::class, 'create']);
    Route::get('/identity/edit/{id}', [IdentityController::class, 'edit']);
    Route::post('/identity/save', [IdentityController::class, 'save']);
    Route::get('/identity/delete/{id}', [IdentityController::class, 'delete']);
    Route::get('/dashboard', [DashboardController::class, 'index']);
    Route::get('/password/index', [PasswordController::class, 'index']);
    Route::post('/password/post', [PasswordController::class, 'post']);
    Route::get('/password/change', [PasswordController::class, 'change']);
    Route::post('/password/change', [PasswordController::class, 'changeHandler']);
    // profile
    Route::get('/reset-password', [ProfileController::class, 'resetPassword']);
    Route::post('/reset-password', [ProfileController::class, 'resetHandler']);
});
//login

Route::middleware(['pakaudit'])->group(function(){
    Route::get('/', [LoginController::class, 'login'])->name('login');
    Route::get('/login', [LoginController::class, 'login']);
    Route::get('/logout', [LoginController::class, 'logout']);
    Route::post('/auth', [LoginController::class, 'auth']);
    Route::get('/verify', [LoginController::class, 'verify']);
    Route::post('/auth_sso', [LoginController::class, 'authsso']);
    Route::get('/logout-sso', [LoginController::class, 'logoutsso']);
    Route::get('/logout-msg', [LoginController::class, 'logoutmsg']);
    Route::get('/logout-all', [LoginController::class, 'logoutall']);
    Route::get('/forgot-password', [ProfileController::class, 'forgot']);
    Route::post('/forgot-password', [ProfileController::class, 'reset']);
    Route::get('/change-password/{token}', [ProfileController::class, 'change']);
    Route::post('/update-password', [ProfileController::class, 'update']);
});


//encrypt test data
Route::get('/encrypt', function() {
    $secret = '123456';
    $secret_iv = '123456';
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $str = openssl_encrypt("test by Maarof ", "AES-256-CBC", "$secret", 0, $iv);
    echo "encrypt = " . $str;
    echo '<hr>';
    $str2 = openssl_decrypt($str,"AES-256-CBC", "123456",0, $iv);
    echo "decrypted = " . $str2;
});

// http://sso.test/gen-password
Route::get('/gen-password', function() {
    echo \Hash::make('1234');
});


Route::get('/diff-date', function() {
    //echo date('Y-m-d');
    //$diff = Carbon::parse('2023-04-01')->diffInDays('2023-04-10');
    //echo $diff;
    // $start = Carbon::now();
    // $end = $start->addDays(-7);
    // echo $start . " " .$end;
    // \App\Models\Respondent::with('survey')
    //     ->whereIn('status', [1, 2])
	//     ->whereDate('email_at','<=', date('Y-m-d', $end));

    $row = DB::select("SELECT DATEDIFF(now(), created_at) FROM identity");
    dd($row);
});
