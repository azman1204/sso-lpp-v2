@extends('layout')
@section('header', 'Senarai Pengguna')
@section('content')

    <a href="/identity/create" class="btn btn-primary mb-1"><span data-feather="plus"></span>Tambah Pengguna</a>

    <div>&nbsp;</div>
    <tr></tr>
    <div></div>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">

                    <form method="post" action="/identity/list" class="mb-2">
                        @csrf

                        <div class="row">
                            <div class="col-md-3">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="name">
                            </div>

                            <div class="col-md-3">
                                <label>No. KP</label>
                                <input type="text" class="form-control" name="user_id">
                            </div>

                            <div class="col-md-1">
                                <div>&nbsp;</div>
                                <input type="submit" class="btn btn-primary" value="Carian">
                            </div>
                        </div>
                        <div>&nbsp;</div>
                    </form>


                    <table class="table table-borded table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Bil</th>
                                <th>Nama</th>
                                <th>Id Pengguna</th>
                                <th>Email</th>
                                <th class="text-center">Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($indentities as $identity)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $identity->name }}</td>
                                    <td>{{ $identity->user_id }}</td>
                                    <td>{{ $identity->email }}</td>
                                    <td align="center">
                                        <a href="/identity/edit/{{ $identity->id }}"
                                            class="btn btn-primary btn-sm">Kemaskini</a>
                                        <a href="/identity/delete/{{ $identity->id }}"
                                            onclick="return confirm('Adakah Anda Pasti ??')"
                                            class="btn btn-danger btn-sm">Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    <div class="mt-2"> {{ $indentities->links()}}

                    </div>

                </div>

            </div>
           
        </div>
    </section>
@endsection
