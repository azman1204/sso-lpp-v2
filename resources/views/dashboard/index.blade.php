@extends('/layout')
@section ('header', 'Capaian Sistem')
@section('content')
    
    <table class="table">
        <thead>
            @foreach ($apps as $app)
            <tr>
                <td>{{$loop->iteration}}.</td>
                <td>
                    <a href="{{ $app->myapp->url}}?user_id={{$user_id}}">{{ $app->myapp->name}}</a>
                </td>
            </tr>
            @endforeach
        </thead>
    </table>
@endsection
