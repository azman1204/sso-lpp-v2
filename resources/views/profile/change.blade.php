@extends('layout_public')
@section('header', 'Change Password')
@section('content')

    <form action="/update-password" method="post">

        <input type="hidden" name="token" value="{{ old('token', $token) }}">
        @csrf
        <div class="row">
            <div class="col-md-4">

                @foreach ($errors->all() as $error)
                    <li class="text-danger">{{ $error }}</li>
                @endforeach

                <label>Katalaluan Baru</label>
                <input type="password" class="form-control" name="password">

                <label>Pengesahan Katalaluan Baru</label>
                <input type="password" class="form-control" name="password_confirmation">

                <input type="submit" class="btn btn-primary">
            </div>
        </div>
    </form>

@endsection
