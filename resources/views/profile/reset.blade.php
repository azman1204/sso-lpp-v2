@extends('layout')
@section('header', 'Reset Password')
@section('content')

    <form action="/reset-password" method="post">
        @csrf
        <div class="row">
            <div class="col-md-4">

                @foreach ($errors->all() as $error)
                    <li class="text-danger">{{ $error }}</li>
                @endforeach

                <label>Katalaluan Baru</label>
                <input type="password" class="form-control" name="password">

                <label>Pengesahan Katalaluan Baru</label>
                <input type="password" class="form-control" name="password_confirmation">

                <input type="submit" class="btn btn-primary">
            </div>
        </div>
    </form>

@endsection
