-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2023 at 04:21 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sso`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit`
--

CREATE TABLE `audit` (
  `id` int(11) NOT NULL,
  `uri` varchar(100) NOT NULL,
  `method` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `data` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `audit`
--

INSERT INTO `audit` (`id`, `uri`, `method`, `ip`, `data`, `created_at`, `updated_at`) VALUES
(1000, '/', 'GET', '127.0.0.1', '[]', '2023-01-14 22:00:47', '2023-01-14 22:00:47'),
(1001, 'auth', 'POST', '127.0.0.1', '{\"_token\":\"1UfQhfuqHkoPHFEXBOaFsRAtHoM7Qv3t3zrJpHWq\",\"user_id\":\"670918120001\"}', '2023-01-14 22:01:01', '2023-01-14 22:01:01'),
(1002, 'login', 'GET', '127.0.0.1', '[]', '2023-01-14 22:01:02', '2023-01-14 22:01:02'),
(1003, 'auth', 'POST', '127.0.0.1', '{\"_token\":\"1UfQhfuqHkoPHFEXBOaFsRAtHoM7Qv3t3zrJpHWq\",\"user_id\":\"670918120001\"}', '2023-01-14 22:01:19', '2023-01-14 22:01:19'),
(1004, 'login', 'GET', '127.0.0.1', '[]', '2023-01-14 22:01:19', '2023-01-14 22:01:19'),
(1005, 'auth', 'POST', '127.0.0.1', '{\"_token\":\"1UfQhfuqHkoPHFEXBOaFsRAtHoM7Qv3t3zrJpHWq\",\"user_id\":\"670918120000\"}', '2023-01-14 22:01:27', '2023-01-14 22:01:27'),
(1006, 'login', 'GET', '127.0.0.1', '[]', '2023-01-14 22:01:27', '2023-01-14 22:01:27');

-- --------------------------------------------------------

--
-- Table structure for table `identity`
--

CREATE TABLE `identity` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `token` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `identity`
--

INSERT INTO `identity` (`id`, `name`, `user_id`, `password`, `email`, `token`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'ali bin abu', '670918120001', '$2y$10$XQHdq5SAvRKuuZlOcGgimeAAjoZfUovhq8oxRkhk9WPSDLq.ND0PG', 'ali@test', 'NjcwOTE4MTIwMDAxMjAyMi0xMS0xMSAxMDo1MzozN3NlY3JldC1rZXk=', 1, 2, '2022-11-08 02:09:54', '2023-01-14 21:52:53', NULL),
(8, 'mohd bin ali', '670918120002', '$2y$10$PYU/EplqEjI.B1UfW6.nBu/SZmKebGPupPzHwkVOjtwvV/le4pRw2', 'mohd@test', NULL, 1, 2, '2022-11-08 02:59:23', '2022-11-11 11:19:13', NULL),
(9, 'siti aminah', '670918120003', '$2y$10$ol.rbtdAsR7LZp7p5ST1J.ZfhaDaUfs/8pRZomC5gZLIL6ekBTYju', 'sitiaminah@tast', '', 1, NULL, '2022-11-08 03:57:24', '2022-11-08 07:04:08', NULL),
(10, 'Hamizan', 'Hamizan', '$2y$10$cTHHN0PCS30JctzWM8LgBu1Jk8kKl3ykn8/aUrGk95nFGLlsWBOlq', 'hamizan@yahoo', '', 1, 2, '2022-11-09 02:28:43', '2022-11-09 02:29:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_by` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `role`, `created_by`, `created_at`, `updated_at`, `deleted_by`) VALUES
(2, 2, 1, 2, '2022-11-10 14:45:44', '2022-11-10 14:45:45', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audit`
--
ALTER TABLE `audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `identity`
--
ALTER TABLE `identity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audit`
--
ALTER TABLE `audit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1007;

--
-- AUTO_INCREMENT for table `identity`
--
ALTER TABLE `identity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
